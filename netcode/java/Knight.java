package netcode.java;

public class Knight {
	private int baseHP;
	private int wp;
	
	public Knight(int baseHP, int wp) {
		if(baseHP < 99 || baseHP > 999 || wp < 0 || wp > 1) {
			throw new IllegalArgumentException("Invalid Value!");
		} else {
			this.baseHP = baseHP;
			this.wp = wp;
		}
		
	}
	
	public int getBaseHP() {
		return baseHP;
	}
	
	public int getwp() {
		return wp;
	}
	
	public int getRealHP() {
		int HP = 0;
		
		if(wp == 0) {
			HP = baseHP / 10;
		} else if(wp == 1) {
			HP = baseHP;
		}
		
		return HP;
	}

}
